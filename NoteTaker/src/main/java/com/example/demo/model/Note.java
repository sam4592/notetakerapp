package com.example.demo.model;

import lombok.Data;

@Data
public class Note {
    
	private Integer note_id;
	private String note_title;
	private String note_context;
	
}
