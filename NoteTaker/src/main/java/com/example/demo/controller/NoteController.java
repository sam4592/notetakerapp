package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Note;
import com.example.demo.service.NoteServiceImp;

@Controller
public class NoteController 
{
	@Autowired
	private NoteServiceImp noteservice;
	
	@GetMapping("/addnote")
	public String loadNoteForm()
	{
		return "Note";
	}
	
	@PostMapping("/addnote")
	public String addNote(Note note, Model model)
	{
		System.out.println( "note details::  " + note.toString());
		
		boolean result = noteservice.addNote(note);
		if(result)
		return "sucess";
		
	return "Note";
	}
	
	@GetMapping("/shownotes")
	public String getAllUsers(Model model)
	{
		List<Note> allNotes= noteservice.getAllNotes();
		System.out.println(allNotes.toString());
		if(allNotes != null)
		{
			model.addAttribute("notes",allNotes);
		}
		return "All_User";
	}

	
	@GetMapping("/updateNote/{id}")
	public String upadteNote(@PathVariable("id") Integer note_id,Model model)
	{ 
		 Note note = noteservice.getNote(note_id);
		 if(note != null)
		 {
			 model.addAttribute("noteValue",note);
			 return "UpdateNote";
		 }
		 model.addAttribute("msg","Note is not updated!!!!!! Please re-try");
		 return "redirect:/shownotes";
		 
	}
	
	@PostMapping("/updateNote/{id}")
	public String upadteNote(Note note,@PathVariable("id") Integer note_id,Model model)
	{
		if(note_id != null && note != null)
		{
			boolean result = noteservice.updateNote(note_id, note);
			if(result)
			{
				model.addAttribute("msg","Sucessfully Updated Note");
				return "redirect:/shownotes";
			}
			
		}
		
		model.addAttribute("msg","Something wents wrong!!! Please re try");
		return "redirect:/shownotes";

	}
	
	@GetMapping("/deletNote/{id}")
	public String deleteNote(@PathVariable("id") Integer note_id,Model model)
	{  
		
		 noteservice.deleleNote(note_id);
		 model.addAttribute("msg","Sucessfully Deleted Note");
		return "redirect:/shownotes";
	}
	
	
	

}
