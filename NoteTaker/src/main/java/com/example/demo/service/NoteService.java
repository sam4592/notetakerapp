package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Note;

public interface NoteService 
{
	
	public boolean addNote(Note note);
	
	public boolean updateNote(Integer id,Note note);
	
	public Note getNote(Integer id);
	
	public boolean deleleNote(Integer note_id);
	
	public List<Note> getAllNotes();

}
