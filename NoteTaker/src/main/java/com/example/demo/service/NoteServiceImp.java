package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.NoteEntity;
import com.example.demo.model.Note;
import com.example.demo.repository.NoteRepository;

@Service
public class NoteServiceImp implements NoteService
{
	@Autowired
	private NoteRepository noteRepo;

	@Override
	public boolean addNote(Note note) 
	{
		if(note == null)
		return false;
		
		NoteEntity noteEntity = new NoteEntity();
		noteEntity.setN_content(note.getNote_context());
		noteEntity.setN_title(note.getNote_title());
		
		NoteEntity entity = noteRepo.save(noteEntity);
		if(entity != null)
		return true;
		
		return false;
	}

	@Override
	public boolean deleleNote(Integer id) 
	{
       if(id != null)
       {
    	   NoteEntity exist = noteRepo.getById(id);
    	   
    	   if(exist != null)
    	   {
    		   noteRepo.deleteById(id);
    		   return true;
    	   }
       }

		return false;
	}

	@Override
	public List<Note> getAllNotes() 
	{
		List<NoteEntity> allNotes = noteRepo.findAll();
	  	if(allNotes != null)
	  	{
	  		List<Note> notes = new ArrayList<Note>();
	  		for (NoteEntity note : allNotes) 
	  		{
			   Note newNote = new Note();
			   newNote.setNote_id(note.getN_id());
			   newNote.setNote_title(note.getN_title());
			   newNote.setNote_context(note.getN_content());
			   notes.add(newNote);
			}
	  		
	  		return notes;
	  	}
		return null;
	}
	
    public boolean updateNote(Integer id,Note note)
    {
    	NoteEntity noteentity = noteRepo.getById(id);
    	if(noteentity != null && note != null)
    	{
    		noteentity.setN_title(note.getNote_title());
    		noteentity.setN_content(note.getNote_context());
    		noteRepo.save(noteentity);
    		return true;
    	}
    	return false;
    }
	
	public Note getNote(Integer id)
	{
		if(id != null)
		{
			NoteEntity noteentity = noteRepo.getById(id);
			
			if(noteentity != null)
			{
				Note note = new Note();
				note.setNote_id(noteentity.getN_id());
				note.setNote_title(noteentity.getN_title());
				note.setNote_context(noteentity.getN_content());
				return note;
			}
			 
			 
			 
		}
		return null;
	}

}
