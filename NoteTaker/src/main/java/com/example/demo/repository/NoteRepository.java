package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.NoteEntity;

public interface NoteRepository extends JpaRepository<NoteEntity, Integer>
{

}
