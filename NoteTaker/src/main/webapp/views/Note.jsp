<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<span>
<label style="font-size: 300%"><b style="color: red;">Note Taker</b></label>&nbsp;&nbsp;&nbsp;
<a href="/">Home</a>&nbsp;&nbsp;
<a href="/addnote">Add Note</a>&nbsp;&nbsp;
<a href="/shownotes">Show Notes</a>&nbsp;&nbsp;
</span>
<h3 style="text-align: center;">Please Fill Your Note Details</h3>
<form action="/addnote" method="post">
<span>Note Title :</span><input type="text" style="width: 400%; height: 200%" name="note_title">
<span>Note Contents :</span><input type="text" name="note_context">
<input type="submit" value="Add"/>
</form>
</body>
</html>