<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<span>
<label style="font-size: 300%"><b style="color: red;">Note Taker</b></label>&nbsp;&nbsp;&nbsp;
<a href="/">Home</a>&nbsp;&nbsp;
<a href="/addnote">Add Note</a>&nbsp;&nbsp;
<a href="/shownotes">Show Notes</a>&nbsp;&nbsp;
</span>
<h3>Please Update Note and Click on Save Button</h3>
<c:form method="POST" action="/updateNote/${noteValue.note_id}">
<input type="text" name="note_title" value="${noteValue.note_title}"/>
<input type="text" name="note_context" value="${noteValue.note_context}"/>
<input type="submit" value="Update And Save"/>
</c:form>
</body>
</html>