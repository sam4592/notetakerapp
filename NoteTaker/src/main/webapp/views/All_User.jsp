<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<span>
<label style="font-size: 300%"><b style="color: red;">Note Taker</b></label>&nbsp;&nbsp;&nbsp;
<a href="/">Home</a>&nbsp;&nbsp;
<a href="/addnote">Add Note</a>&nbsp;&nbsp;
</span>
<h4>${msg}</h4>
<c:forEach items="${notes}" var="note">
<div>
<div>
${note.note_title}
</div>
<div>
${note.note_context}
</div>
<span><a href="/updateNote/${note.note_id}">Update</a></span>
<span><a href="/deletNote/${note.note_id}">Delete</a></span>
</div>
</c:forEach>
</body>
</html>